import { IItem, TierOption } from '@/types';

export const getTierItems = (tier: TierOption, items: IItem[]) => {
  return items.filter((item) => item.remainingQuantity > 0 && item.tier === tier);
};
