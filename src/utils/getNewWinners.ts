import { IItem, IWinners } from '@/types';
import { getUniqueItemId } from './getUniqueItemId';

export const getNewWinners = (oldWinners: IWinners, winningItems: IItem[], username: string) => {
  const copy = { ...oldWinners };
  winningItems.forEach((item) => {
    const uniqueId = getUniqueItemId(item);
    copy.winners[uniqueId] = copy.winners[uniqueId] ? [...copy.winners[uniqueId], username] : [username];
  });

  return copy;
};
