import { IItem } from '@/types';
import { getGifts, getItems, getWinners, writeGifts, writeItems, writeWinners } from './file-helper';
import { getNewGifts } from './getNewGifts';
import { getNewItems } from './getNewItems';
import { getNewWinners } from './getNewWinners';
import { getRandomInt } from './getRandomInt';
import { getTierItems } from './getTierItems';
import { getTierRolls } from './getTierRolls';
import { getAvailabilitySummary } from './getAvailabilitySummary';

export const rollForUser = (username: string) => {
  const items = getItems();
  const tierRolls = getTierRolls(items.items);
  const winningItems: IItem[] = [];
  tierRolls.winners.forEach((tier) => {
    const tierItems = getTierItems(tier, items.items);
    winningItems.push(tierItems[getRandomInt(0, tierItems.length - 1)]);
  });

  const newItems = getNewItems(items, winningItems);

  writeItems(newItems);
  writeWinners(getNewWinners(getWinners(), winningItems, username));
  writeGifts(getNewGifts(getGifts(), username, winningItems, tierRolls.rolls));

  console.log('Availability: ', getAvailabilitySummary(newItems));
};
