import { IGifts, IItem, ITierRoll } from '@/types';

export const getNewGifts = (oldGifts: IGifts, username: string, winningItems: IItem[], rolls: ITierRoll[]) => {
  const copy = { ...oldGifts };
  copy.gifts.push({
    username,
    wins: winningItems,
    rolls,
  });
  return copy;
};
