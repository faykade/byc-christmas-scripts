import { IItem } from '@/types';
import { getTierSort } from './getTierSort';

export const getAvailableTiers = (items: IItem[]) => {
  return Object.keys(
    items.filter((item) => item.remainingQuantity > 0).reduce((acc, curr) => ({ ...acc, [curr.tier]: true }), {}),
  ).sort((a, b) => getTierSort(a, b));
};
