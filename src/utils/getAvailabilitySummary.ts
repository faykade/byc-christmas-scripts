import { TIERS } from '@/constants';
import { IItems } from '@/types';
import { getTierItems } from './getTierItems';

export const getAvailabilitySummary = ({ items = [] }: IItems) => {
  const ret: Record<string, number> = {};
  TIERS.forEach((tier) => {
    ret[tier.tier] = getTierItems(tier.tier, items).length;
  });

  return ret;
};
