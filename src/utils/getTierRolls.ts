import { IItem, ITierRolls, TierOption } from '@/types';
import { getTierThresholds } from './getTierThresholds';
import { getRandomInt } from './getRandomInt';
import { getAvailableTiers } from './getAvailableTiers';

const MIN_WINS = 3;

export const getTierRolls = (items: IItem[]): ITierRolls => {
  const thresholds = getTierThresholds();
  const ret: ITierRolls = {
    winners: [],
    rolls: [],
  };

  const availableTiers = getAvailableTiers(items);

  let currentRoll = -1;
  let isWinner = false;
  const realMinWinCount = Math.min(availableTiers.length, MIN_WINS);
  for (let i = 0; ret.winners.length < realMinWinCount; i++) {
    currentRoll = getRandomInt(1, 100);
    availableTiers.forEach((tier) => {
      if (!ret.winners.includes(tier as TierOption)) {
        isWinner = currentRoll >= thresholds[tier];
        if (isWinner) {
          ret.winners.push(tier as TierOption);
        }
        ret.rolls.push({
          tierOption: tier as TierOption,
          roll: currentRoll,
          winner: isWinner,
        });
      }
    });
  }

  return ret;
};
