import { IItem, IItems } from '@/types';
import { getTierSort } from './getTierSort';
import { getUniqueItemId } from './getUniqueItemId';

export const getNewItems = (oldItems: IItems, itemsWon: IItem[]): IItems => {
  return {
    items: oldItems.items
      .map((item) =>
        itemsWon.map((wonItem) => getUniqueItemId(wonItem)).includes(getUniqueItemId(item))
          ? { ...item, remainingQuantity: item.remainingQuantity - 1 }
          : item,
      )
      .sort((a, b) => getTierSort(a.tier, b.tier)),
  };
};
