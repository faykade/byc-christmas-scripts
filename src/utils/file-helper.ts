import * as giftsJson from '@/assets/gifts.json';
import * as itemsJson from '@/assets/items.json';
import * as winnersJson from '@/assets/winners.json';
import { join } from 'path';
import { writeFileSync } from 'fs';

import { IGifts, IItems, IWinners } from '@/types';

const getSrcAssetsPath = (filename: string): string => {
  return join(__dirname, '..', '..', 'src', 'assets', filename);
};

// eslint-disable-next-line
const getData = (contents: any) => {
  return JSON.stringify(contents, undefined, 2);
};

export const getItems = (): IItems => {
  return itemsJson as IItems;
};

export const getGifts = (): IGifts => {
  return giftsJson as IGifts;
};

export const getWinners = (): IWinners => {
  return winnersJson as IWinners;
};

export const writeItems = (items: IItems) => {
  writeFileSync(getSrcAssetsPath('items.json'), getData(items), { flag: 'w' });
};

export const writeGifts = (gifts: IGifts) => {
  writeFileSync(getSrcAssetsPath('gifts.json'), getData(gifts), { flag: 'w' });
};

export const writeWinners = (winners: IWinners) => {
  writeFileSync(getSrcAssetsPath('winners.json'), getData(winners), { flag: 'w' });
};
