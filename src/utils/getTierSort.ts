export const getTierSort = (a: string, b: string) => {
  if (a === 'S' && b === 'S') {
    return 0;
  } else if (a === 'S') {
    return -1;
  } else if (b === 'S') {
    return 1;
  } else {
    return a.localeCompare(b);
  }
};
