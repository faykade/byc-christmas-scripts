import { ITier } from '@/types';

export const S_TIER: ITier = {
  tier: 'S',
  rollThreshold: 30,
};

export const A_TIER: ITier = {
  tier: 'A',
  rollThreshold: 40,
};

export const B_TIER: ITier = {
  tier: 'B',
  rollThreshold: 60,
};

export const C_TIER: ITier = {
  tier: 'C',
  rollThreshold: 65,
};

export const D_TIER: ITier = {
  tier: 'D',
  rollThreshold: 70,
};

export const F_TIER: ITier = {
  tier: 'F',
  rollThreshold: 80,
};

export const TIERS = [S_TIER, A_TIER, B_TIER, C_TIER, D_TIER, F_TIER];
