import { rollForUser } from './utils';

const params = process.argv.splice(2);

if (params.length) {
  const username = params[0];
  rollForUser(username);
} else {
  console.log(`Usage: npm run roll [username]`);
}
