import { IItem } from './IItem';

export type IItems = Record<'items', IItem[]>;
