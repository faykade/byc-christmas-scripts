import { TierOption } from './TierOption';

export interface IItem {
  id: number;
  tier: TierOption;
  giftQuantity: number;
  remainingQuantity: number;
}
