import { IItem } from './IItem';
import { ITierRoll } from './ITierRoll';

export interface IGift {
  username: string;
  wins: IItem[];
  rolls: ITierRoll[];
}
