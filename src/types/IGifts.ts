import { IGift } from './IGift';

export type IGifts = Record<'gifts', IGift[]>;
