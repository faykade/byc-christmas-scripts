import { TierOption } from './TierOption';

export interface ITierRoll {
  tierOption: TierOption;
  roll: number;
  winner: boolean;
}
