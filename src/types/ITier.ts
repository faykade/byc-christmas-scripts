import { TierOption } from './TierOption';

export interface ITier {
  tier: TierOption;
  rollThreshold: number;
}
