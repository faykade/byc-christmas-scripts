import { ITierRoll } from './ITierRoll';
import { TierOption } from './TierOption';

export interface ITierRolls {
  rolls: ITierRoll[];
  winners: TierOption[];
}
